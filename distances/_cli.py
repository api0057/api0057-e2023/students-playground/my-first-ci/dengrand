import numpy as np
import argparse

from distnces.distance import haussdorff, minimal


def _parser():
    parser = argparse.ArgumentParser(
        prog="distance_calculator",
        description="Calculate distances between points.",
    )

    parser.add_argument("X_points")
    parser.add_argument("Y_points")
    parser.add_argument(
        "-d", "--distance", help="Distance calculation mode", default="minimal"
    )
    return parser


def main():
    parser = _parser()
    args = parser.parse_args()

    x_points = np.loadtxt(args.x_points)
    y_points = np.loadtxt(args.y_points)

    if args.distance == "minimal":
        print(minimal(x_points, y_points))
    elif args.distance == "hausdorff":
        print(hausdorff(x_points, y_points))
